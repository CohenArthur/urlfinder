CC=gcc
CFLAGS= -Wall -Wextra -Werror -g -std=c99
LDLIBS= -lcurl

SRC=urlfinder.c
OBJ=${SRC:*.c=*.o}

all: urlfinder

urlfinder: ${OBJ}

clean:
	${RM} urlfinder *.o *.d
